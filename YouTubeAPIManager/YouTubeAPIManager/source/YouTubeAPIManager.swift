//
//  YouTubeAPIManager.swift
//  YouTubeAPIManager
//
//  Copyright © 2018 Muhammad Bassio. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import Foundation
import RESTKit
import OAuthKit

public enum YouTubeScope:String {
  case all = "https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtube.readonly https://www.googleapis.com/auth/youtubepartner https://www.googleapis.com/auth/youtubepartner-channel-audit https://www.googleapis.com/auth/youtube.upload"
  case partner = "https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtube.readonly https://www.googleapis.com/auth/youtubepartner  https://www.googleapis.com/auth/youtubepartner-channel-audit"
  case readOnly = "https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtube.readonly"
  case upload = "https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtube.readonly www.googleapis.com/auth/youtube.upload"
}

open class YouTubeAPIManager: APIManager {
  
  private(set) var bundleId = ""
  private(set) public var authClient: OAuth2Client
  private var channelThumbs:[String:String] = [:]
  
  var endpoints:EndPoints = EndPoints(apiKey: "")
  
  // global variable for dates with format "yyyy-MM-dd"
  static let dateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    return dateFormatter
  }()
  
  public init(bundleId:String, apiKey:String, clientId:String, redirectURL:String) {
    self.bundleId = bundleId
    self.endpoints = EndPoints(apiKey: apiKey)
    let config = APIConfiguration()
    SessionManager.default.session.configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
    config.baseURL = "https://www.googleapis.com/youtube/v3"
    config.mainHeaders = ["X-Ios-Bundle-Identifier":self.bundleId]
    self.authClient = OAuth2Client(configuration: OAuth2Configuration(clientId: clientId, authURL: "https://accounts.google.com/o/oauth2/auth", tokenURL: "https://www.googleapis.com/oauth2/v4/token", scope: YouTubeScope.all.rawValue, redirectURL: redirectURL, responseType: "code"))
    super.init(configuration: config)
    self.loadSavedToken()
  }
  
  public init(bundleId:String, apiKey:String, clientId:String, redirectURL:String, scope:YouTubeScope) {
    self.bundleId = bundleId
    self.endpoints = EndPoints(apiKey: apiKey)
    SessionManager.default.session.configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
    let config = APIConfiguration()
    config.baseURL = "https://www.googleapis.com/youtube/v3"
    config.mainHeaders = ["X-Ios-Bundle-Identifier":self.bundleId]
    self.authClient = OAuth2Client(configuration: OAuth2Configuration(clientId: clientId, authURL: "https://accounts.google.com/o/oauth2/auth", tokenURL: "https://www.googleapis.com/oauth2/v4/token", scope: scope.rawValue, redirectURL: redirectURL, responseType: "code"))
    super.init(configuration: config)
    self.loadSavedToken()
  }
  
  override open func loadSavedToken() {
    self.accessToken = self.authClient.token
  }
  
  open func login(viewController:UIViewController) {
    self.authClient.authorize(from: viewController)
  }
  
  open func logout() {
    self.authClient.unauthorize()
  }
  
  
  
  
  public func requestResults(endpoint:APIEndPoint, completion:@escaping (_ resultsArray:Array<JSON>, _ nextToken:String, _ errorString:String) -> Void) {
    super.sendRequest(endPoint: endpoint) { response in
      if let data = response.data {
        let json = JSON(data)
        if json["error"].exists() {
          let error = json["error"]
          if error["code"].intValue == 401 {
            self.authClient.refreshAccessToken()
          } else {
            completion([], "", "\(error)")
          }
        } else {
          let pageToken = json["nextPageToken"].stringValue
          if let itms = json["items"].array {
            let results = itms
            completion(results, pageToken, "")
          }
          else {
            print("no items!")
            completion([], "", "No items!")
          }
        }
      }
    }
  }
  
  
}
