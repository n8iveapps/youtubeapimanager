//
//  EndPoints.swift
//  YouTubeAPIManager
//
//  Copyright © 2018 Muhammad Bassio. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import Foundation
import OAuthKit
import RESTKit

open class EndPoints {
  
  private(set) var apiKey = ""
  
  public init(apiKey:String) {
    self.apiKey = apiKey
  }
  
  public func searchResultsEndpoint(for params:Dictionary<String,String>!, maxResults:Int) -> APIEndPoint {
    var path:String = "search?part=id&key=\(self.apiKey)&maxResults=\(maxResults)"
    for (key,val) in params {
      path = "\(path)&\(key)=\(val)"
    }
    return APIEndPoint(path: path, method: .get, requiresAuth: false)
  }
  
  
  public func videosEndpoint(for part: String = "snippet,contentDetails", params:Dictionary<String,String>!, maxResults:Int) -> APIEndPoint {
    var path:String = "videos?part=\(part)&key=\(self.apiKey)&maxResults=\(maxResults)"
    for (key,val) in params {
      path = "\(path)&\(key)=\(val)"
    }
    return APIEndPoint(path: path, method: .get, requiresAuth: false)
  }
  
  public func playlistsEndpoint(for part: String = "snippet,status,contentDetails", params:Dictionary<String,String>!, maxResults:Int) -> APIEndPoint {
    var path:String = "playlists?part=\(part)&key=\(self.apiKey)&maxResults=\(maxResults)"
    for (key,val) in params {
      path = "\(path)&\(key)=\(val)"
    }
    return APIEndPoint(path: path, method: .get, requiresAuth: false)
  }
  
  public func playlistItemsEndpoint(for part: String = "snippet", params:Dictionary<String,String>!, maxResults:Int) -> APIEndPoint {
    var path:String = "playlistItems?part=\(part)&key=\(self.apiKey)&maxResults=\(maxResults)"
    for (key,val) in params {
      path = "\(path)&\(key)=\(val)"
    }
    return APIEndPoint(path: path, method: .get, requiresAuth: false)
  }
  
  public func subscriptionsEndpoint(for part: String = "snippet", params:Dictionary<String,String>!, maxResults:Int) -> APIEndPoint {
    var path:String = "subscriptions?part=\(part)&key=\(self.apiKey)&maxResults=\(maxResults)"
    for (key,val) in params {
      path = "\(path)&\(key)=\(val)"
    }
    return APIEndPoint(path: path, method: .get, requiresAuth: true)
  }
  
  public func channelsEndpoint(for part: String = "snippet", params:Dictionary<String,String>!, maxResults:Int) -> APIEndPoint {
    var path:String = "channels?part=\(part)&key=\(self.apiKey)&maxResults=\(maxResults)"
    for (key,val) in params {
      path = "\(path)&\(key)=\(val)"
    }
    return APIEndPoint(path: path, method: .get, requiresAuth: false)
  }
  
  public func channelSectionsEndpoint(for part: String = "snippet", params:Dictionary<String,String>!, maxResults:Int) -> APIEndPoint {
    var path:String = "channelSections?part=\(part)&key=\(self.apiKey)&maxResults=\(maxResults)"
    for (key,val) in params {
      path = "\(path)&\(key)=\(val)"
    }
    return APIEndPoint(path: path, method: .get, requiresAuth: false)
  }
  
  public func activitiesEndpoint(for part: String = "snippet", params:Dictionary<String,String>!, maxResults:Int) -> APIEndPoint {
    var path:String = "activities?part=\(part)&key=\(self.apiKey)&maxResults=\(maxResults)"
    for (key,val) in params {
      path = "\(path)&\(key)=\(val)"
    }
    return APIEndPoint(path: path, method: .get, requiresAuth: true)
  }
  
  
}
